//
//  SecondViewController.swift
//  ObserverArchitecture
//
//  Created by Raphael Berendes on 20.04.19.
//  Copyright © 2019 Raphael Berendes. All rights reserved.
//

import UIKit

class SecondViewControllerClosure: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        AudioPlayerClosure.shared.observePlaybackStarted { (player, item) in
            print(self)
        }
    }
    
    deinit {
        print("Deinit \(self)")
    }
    
}
