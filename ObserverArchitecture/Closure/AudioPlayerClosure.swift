//
//  AudioPlayer.swift
//  ObserverArchitecture
//
//  Created by Raphael Berendes on 20.04.19.
//  Copyright © 2019 Raphael Berendes. All rights reserved.
//

import UIKit
import AVFoundation

struct Item {
    let title: String
    let duration: String
}

class AudioPlayerClosure {
    
    static let shared = AudioPlayerClosure()
    
    private init() {}
    
    
    var player: AVAudioPlayer?
    
    private var observations = (
        started: [(AudioPlayerClosure, Item) -> Void](),
        paused: [(AudioPlayerClosure, Item) -> Void](),
        stopped: [(AudioPlayerClosure) -> Void]()
    )
    
    private var state = State.idle {
        didSet { stateDidChange() }
    }
    
    func play(_ item: Item) {
        state = .playing(item)
        startPlayback(with: item)
    }
    
    func pause() {
        switch state {
        case .idle, .paused:
            // Calling pause when we're not in a playing state
            // could be considered a programming error, but since
            // it doesn't do any harm, we simply break here.
            break
        case .playing(let item):
            state = .paused(item)
            pausePlayback()
        }
    }
    
    func stop() {
        state = .idle
        stopPlayback()
    }
    
    private func startPlayback(with item: Item) {
        
        if player != nil {
            player?.play()
            
            return
        }
        
        guard let asset = NSDataAsset(name: item.title) else {
            return
        }
        
        do {
            player = try AVAudioPlayer(data: asset.data)
            guard let player = player else { return }
            player.play()
        } catch {
            print(error)
        }
    }
    
    private func pausePlayback() {
        player?.pause()
    }
    
    private func stopPlayback() {
        player?.stop()
        player = nil
    }

}

extension AudioPlayerClosure {
    
    func observePlaybackStarted(using closure: @escaping (AudioPlayerClosure, Item) -> Void) {
        observations.started.append(closure)
    }
    
    func observePlaybackPaused(using closure: @escaping (AudioPlayerClosure, Item) -> Void) {
        observations.paused.append(closure)
    }
    
    func observePlaybackStopped(using closure: @escaping (AudioPlayerClosure) -> Void) {
        observations.stopped.append(closure)
    }
}

private extension AudioPlayerClosure {
    
    func stateDidChange() {
        switch state {
        case .idle:
            observations.stopped.forEach { closure in
                closure(self)
            }
        case .playing(let item):
            observations.started.forEach { closure in
                closure(self, item)
            }
        case .paused(let item):
            observations.paused.forEach { closure in
                closure(self, item)
            }
        }
    }
}

private extension AudioPlayerClosure {
    
    enum State {
        case idle
        case playing(Item)
        case paused(Item)
    }
}
