//
//  ViewController.swift
//  ObserverArchitecture
//
//  Created by Raphael Berendes on 20.04.19.
//  Copyright © 2019 Raphael Berendes. All rights reserved.
//

import UIKit

class FirstViewControllerClosure: UIViewController {
    
    let package = Item(title: "package", duration: "7:31")
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()

        AudioPlayerClosure.shared.observePlaybackStarted { (player, item) in
            print(self)
        }
        
        
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { timer in
            self.play(UIButton())
        })
    }

    @IBAction func play(_ sender: UIButton) {
        AudioPlayerClosure.shared.play(package)
    }
    
    @IBAction func pause(_ sender: UIButton) {
        AudioPlayerClosure.shared.pause()
    }
    
    @IBAction func stop(_ sender: UIButton) {
        AudioPlayerClosure.shared.stop()
    }
    
}
