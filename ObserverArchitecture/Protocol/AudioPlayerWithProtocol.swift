//
//  AudioPlayerWithProtocol.swift
//  ObserverArchitecture
//
//  Created by Raphael Berendes on 20.04.19.
//  Copyright © 2019 Raphael Berendes. All rights reserved.
//

import UIKit
import AVFoundation

class AudioPlayerWithProtocol {
    
    enum State {
        case idle
        case playing(Item)
        case paused(Item)
    }
    
    static let shared = AudioPlayerWithProtocol()
    var player: AVAudioPlayer?
    var state = State.idle {
        didSet { stateDidChange() }
    }
    
    struct Observation {
        weak var observer: AudioPlayerObserver?
    }
    private var observations = [ObjectIdentifier : Observation]()

    
    // MARK: Initializers
    
    private init() {}
    
    
    // MARK: Player public methods
    
    func play(_ item: Item) {
        
        state = .playing(item)
        
        if player != nil {
            player?.play()
        } else {
            startPlayback(with: item)
        }
    }
    
    func pause() {
        
        switch state {
        case .idle, .paused:
            break
        case .playing(let item):
            state = .paused(item)
            pausePlayback()
        }
    }
    
    func stop() {
        
        state = .idle
        stopPlayback()
    }
    
    
    // MARK: Player private methods
    
    private func startPlayback(with item: Item) {
        
        guard let asset = NSDataAsset(name: item.title) else {
            return
        }
        
        do {
            player = try AVAudioPlayer(data: asset.data)
            guard let player = player else { return }
            player.play()
        } catch {
            print(error)
        }
    }
    
    private func pausePlayback() {
        player?.pause()
    }
    
    private func stopPlayback() {
        
        player?.stop()
        player = nil
    }
    
}

extension AudioPlayerWithProtocol {
    
    func addObserver(_ observer: AudioPlayerObserver) {
        let id = ObjectIdentifier(observer)
        observations[id] = Observation(observer: observer)
    }
    
    private func stateDidChange() {
        
        for (id, observation) in observations {
            // If the observer is no longer in memory, we
            // can clean up the observation for its ID
            guard let observer = observation.observer else {
                observations.removeValue(forKey: id)
                continue
            }
            
            switch state {
            case .idle:
                observer.audioPlayerDidStop(self)
            case .playing(let item):
                observer.audioPlayer(self, didStartPlaying: item)
            case .paused(let item):
                observer.audioPlayer(self, didPausePlaybackOf: item)
            }
        }
    }
    
}

