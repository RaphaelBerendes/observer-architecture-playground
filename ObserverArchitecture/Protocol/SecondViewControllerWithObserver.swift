//
//  SecondViewControllerWithObserver.swift
//  ObserverArchitecture
//
//  Created by Raphael Berendes on 20.04.19.
//  Copyright © 2019 Raphael Berendes. All rights reserved.
//

import UIKit

class SecondViewControllerWithObserver: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        AudioPlayerWithProtocol.shared.addObserver(self)
    }
    
    deinit {
        print("Deinit \(self)")
    }
    
}

extension SecondViewControllerWithObserver: AudioPlayerObserver {
    
    func audioPlayer(_ player: AudioPlayerWithProtocol, didStartPlaying item: Item) {
        print(self)
    }
}
