//
//  AudioPlayerObserver.swift
//  ObserverArchitecture
//
//  Created by Raphael Berendes on 20.04.19.
//  Copyright © 2019 Raphael Berendes. All rights reserved.
//

protocol AudioPlayerObserver: class {
    
    func audioPlayer(_ player: AudioPlayerWithProtocol,
                     didStartPlaying item: Item)
    
    func audioPlayer(_ player: AudioPlayerWithProtocol,
                     didPausePlaybackOf item: Item)
    
    func audioPlayerDidStop(_ player: AudioPlayerWithProtocol)
}

extension AudioPlayerObserver {
    
    func audioPlayer(_ player: AudioPlayerWithProtocol,
                     didStartPlaying item: Item) {}
    
    func audioPlayer(_ player: AudioPlayerWithProtocol,
                     didPausePlaybackOf item: Item) {}
    
    func audioPlayerDidStop(_ player: AudioPlayerWithProtocol) {}
}
