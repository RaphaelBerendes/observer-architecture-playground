//
//  FirstViewControllerWithObserver.swift
//  ObserverArchitecture
//
//  Created by Raphael Berendes on 20.04.19.
//  Copyright © 2019 Raphael Berendes. All rights reserved.
//

import UIKit

class FirstViewControllerWithObserver: UIViewController {
    
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()

        AudioPlayerWithProtocol.shared.addObserver(self)
        
        timer = Timer.scheduledTimer(withTimeInterval: 8, repeats: true, block: { _ in
            AudioPlayerWithProtocol.shared.play(Item(title: "package", duration: "7:31"))
        })
        timer?.fire()
    }
    
}

extension FirstViewControllerWithObserver: AudioPlayerObserver {
    
    func audioPlayer(_ player: AudioPlayerWithProtocol, didStartPlaying item: Item) {
        print(self)
    }
}
